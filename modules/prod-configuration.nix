{ lib, modulesPath, ... }:
{
  networking = {
    hostName = "oecis";
    interfaces = {
      ens3.ipv6.addresses = [
        {
          address = "2a03:4000:65:dd7::69";
          prefixLength = 64;
        }
      ];
    };
    defaultGateway6 = {
      address = "fe80::1";
      interface = "ens3";
    };
    wireguard = {
      interfaces = {
        wg0 = {
          privateKeyFile = "/etc/wireguard/private.key";
          peers = [
            {
              publicKey = "0w9m78GrI/wQVFtv26fphsRaNabZqT2ntLwXfp2H5Dc=";
              allowedIPs = [ "10.69.0.2/32" ];
            }
            {
              publicKey = "/0c3Wv/qDArYKp4R55pXfVeLLNobBvMSUeF7ClKHsgI=";
              allowedIPs = [ "10.69.0.3/32" ];
            }
            {
              publicKey = "o79l/jJFSKRidJbJfY5kR62QVPeyo3dq+q8MGW47i2Y=";
              allowedIPs = [ "10.69.0.4/32" ];
            }
          ];
        };
      };
    };
  };

  users.users.boi = {
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBYPXxw6LeYfKv315ZR69dRCukbrYehCH7gTEe/hdfS7 jrester379@gmail.com"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINOp2utSOkHcJE8tjXlfr5dCHMPmcvnTcCWZLhWNdqUC manuel@archlinux"
    ];
  };

  services.k3s = {
    nodeIpv4 = "89.58.38.24";
    nodeExternalIpv4 = "89.58.38.24";
    nodeIpv6 = "2a03:4000:65:dd7::70";
    nodeExternalIpv6 = "2a03:4000:65:dd7::70";
    clusterDomain = "oecis.io";
  };

  # hardware configuration
  imports = [ (modulesPath + "/profiles/qemu-guest.nix") ];

  boot.initrd.availableKernelModules = [
    "ata_piix"
    "uhci_hcd"
    "virtio_pci"
    "sr_mod"
    "virtio_blk"
  ];
  boot.initrd.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/27335a87-1305-4b1c-af66-ee70e6de1a6f";
    fsType = "ext4";
  };

  swapDevices = [ ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.ens3.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
