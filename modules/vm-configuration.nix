# hardware-configuration for the local dev VM

{ ... }:

{
  networking = {
    extraHosts = ''
      10.0.2.15 minio.oecis.test
      10.0.2.15 traefik.oecis.test
      10.0.2.15 vault.oecis.test
      10.0.2.15 search.oecis.test
      10.0.2.15 oecis.test
      10.0.2.15 kratos.oecis.test
      10.0.2.15 hydra.oecis.test
      10.0.2.15 registry.oecis.test
      	'';
    hostName = "oecis";
    interfaces = {
      ens3 = {
        ipv4.addresses = [
          {
            address = "10.0.2.15";
            prefixLength = 24;
          }
        ];
        ipv6.addresses = [
          {
            # https://wiki.gentoo.org/wiki/QEMU/KVM_IPv6_Support#IPv6_address_and_default_route
            address = "2001:db8:dead:beef:fe::69";
            prefixLength = 96;
          }
        ];
      };
    };
    defaultGateway = {
      address = "10.0.2.2";
      interface = "ens3";
    };
    defaultGateway6 = {
      address = "fe80::2";
      interface = "ens3";
    };
    wireguard.interfaces.wg0.privateKeyFile = "/root/wireguard/private.key";
  };
  services.k3s = {
    nodeIpv4 = "10.0.2.15";
    nodeExternalIpv4 = "10.0.2.15";
    nodeIpv6 = "2001:db8:dead:beef:fe::69";
    nodeExternalIpv6 = "2001:db8:dead:beef:fe::69";
    clusterDomain = "oecis.test";
  };

  # users.users.boi = {
  #   openssh.authorizedKeys = {
  #     keyFiles = [ ~/.ssh/id_ed25519.pub ];
  #   };
  # };

  # To mimick the production system as close as possible the virtual interface is renamed to ens3
  services.udev.extraRules = "	SUBSYSTEM==\"net\", ACTION==\"add\", ATTR{address}==\"52:54:00:12:34:56\", NAME=\"ens3\"\n";
  virtualisation = {
    vmVariant = {
      # following configuration is added only when building VM with build-vm
      virtualisation = {
        memorySize = 10096;
        cores = 4;
        diskSize = 15000;
        sharedDirectories = {
          minio = {
            target = "/root/minio";
            source = ''"$CONFIG_DIR"/minio'';
          };
        };
        forwardPorts = [
          {
            from = "host";
            host.port = 42069;
            guest.port = 42069;
          }
          {
            from = "host";
            host.port = 80;
            guest.port = 80;
          }
          {
            from = "host";
            host.port = 443;
            guest.port = 443;
          }
          {
            from = "host";
            host.port = 35920;
            guest.port = 35920;
          }
          {
            from = "host";
            host.port = 35921;
            guest.port = 35921;
          }
          {
            from = "host";
            host.port = 6443;
            guest.port = 6443;
          }
          {
            from = "host";
            proto = "udp";
            host.port = 51820;
            guest.port = 51820;
          }
        ];
      };
    };
  };
}
