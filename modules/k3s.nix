{ config, lib, ... }:
with lib;
let
  cfg = config.services.k3s;
in
{
  options.services.k3s = {
    nodeIpv4 = mkOption { type = types.str; };
    nodeIpv6 = mkOption { type = types.str; };
    nodeExternalIpv4 = mkOption { type = types.str; };
    nodeExternalIpv6 = mkOption { type = types.str; };
    clusterDomain = mkOption { type = types.str; };
  };
  config = {
    services.k3s.extraFlags = ''
      --node-name=oecis.io \
      --node-label='cilium-firewall=true' \
      --disable-network-policy \
      --flannel-backend=none \
      --disable-kube-proxy \
      --node-ip=${cfg.nodeIpv4},${cfg.nodeIpv6} \
      --node-external-ip=${cfg.nodeExternalIpv4},${cfg.nodeExternalIpv6} \
      --kubelet-arg="node-ip=::" \
      --disable=traefik,servicelb,metrics-server \
      --cluster-cidr=10.42.0.0/16,2001:cafe:42:0::/56 \
      --service-cidr=10.43.0.0/16,2001:cafe:42:1::/112'';
  };
}
