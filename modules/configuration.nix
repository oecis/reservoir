# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ pkgs, ... }:

{
  disabledModules = [ "services/web-servers/minio.nix" ];
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda"; # or "nodev" for efi only
  # Modules required by cilium for IPv6 support
  boot.kernelModules = [
    "ip6_tables"
    "ip6table_mangle"
    "ip6table_raw"
    "ip6table_filter"
  ];
  boot.kernel.sysctl = {
    # Fixes for k8up
    "fs.inotify.max_user_watches" = 524288;
    "fs.inotify.max_user_instances" = 8192;
    "fs.inotify.max_queued_events" = 16384;
  };

  networking = {
    nameservers = [ "9.9.9.9" ];
    wireguard.interfaces = {
      wg0 = {
        ips = [ "10.69.0.1/24" ];
        listenPort = 51820;
      };
    };
  };

  time.timeZone = "Europe/Berlin";

  i18n.defaultLocale = "en_US.UTF-8";

  users.users.boi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    initialPassword = "password";
  };

  environment.systemPackages = with pkgs; [
    vim
    helix
    kubectl
    dig
    tcpdump
    iptables
    nfs-utils
    btop
  ];

  services.openssh = {
    enable = true;
    ports = [ 42069 ];
    settings.PasswordAuthentication = false;
  };

  environment.etc = {
    k3s = {
      target = "rancher/k3s/config.yaml";
      text = builtins.toJSON {
        kubelet-arg = [
          "--eviction-hard=memory.available<200Mi,nodefs.available<500Mi,imagefs.available<500Mi"
        ];
      };
    };
  };

  services.k3s = {
    enable = true;
    configPath = "/etc/rancher/k3s/config.yaml";
  };

  services.minio = {
    region = "eu-central-1";
    enable = true;
    listenAddress = ":35920";
    consoleAddress = ":35921";
    rootCredentialsFile = "/root/minio/minio-root-credentials";
    certsDir = "/var/lib/minio/config/certs";
  };

  # NFS fixes
  boot.supportedFilesystems = [ "nfs" ];
  services.rpcbind.enable = true;

  # Disable the firewall because kubernetes will take care of this
  networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
