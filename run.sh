sudo modprobe nfs
sudo modprobe nfsd
docker-compose up -d

export CONFIG_DIR=$(pwd)/nixos


nixos-rebuild --flake .#test build-vm
sudo -E ./result/bin/run-oecis-vm

# Shutdown the containers if the VM is stopped
docker-compose down
