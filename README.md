# reservoir

## Development

### Prerequisits

* nix
* qemu
* docker
* docker-compose

On arch:
```
sudo pacman -S docker docker-compose qemu-base nix
```

### Run all components required for development

Getting started is as easy as executing the following command. This will load required kernel modules, start docker containers, build the VM and start it.

```
$ ./run.sh
```

To stop press ctrl+c which will shutdown the VM and the containers.

The default login for the VM is `boi` and `password`.

### S3 config

After the VM is up and running you need to add the minio credentials to your S3 credentials file:

```
# ~/.aws/credentials
---
[dev]
aws_access_key_id = boi
aws_secret_access_key = password
```

### k3s config

To access the k3s cluster running in the VM you must first extract the credentials from the VM and place them on your system.
The default kubeconfig is stored at `/etc/rancher/k3s/k3s.yaml`. You can easily access it by ssh into the vm: `ssh boi@localhost -p 42069`
You need to copy the config from there to your local system. For this you have two options:
1. Just copy the file to `~/.kube/config` on your host (this is recommended if you don't already have a kube config locally)
2. extend your existing one and rename the resources accordingly (e.g. dev for the VM), like in the example below (you should choose this, if you already have a kube config locally)


```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: <cert>
    server: https://<prod server ip>:6443
  name: prod
- cluster:
    certificate-authority-data: <cert>
    server: https://127.0.0.1:6443
  name: dev
contexts:
- context:
    cluster: prod
    user: prod
  name: prod
- context:
    cluster: dev
    user: dev
  name: dev
current-context: prod
kind: Config
preferences: {}
users:
- name: prod
  user:
    client-certificate-data: <cert>
    client-key-data: <key>
- name: dev
  user:
    client-certificate-data: <cert>
    client-key-data: <key>
```


### Domain overrides

You need to enable custom static mappings for the `oecis.test` domain. The most straightforward way is to modify `/etc/hosts` and adding the following entries:

```
127.0.0.1  minio.oecis.test
127.0.0.1  traefik.oecis.test
127.0.0.1  vault.oecis.test
127.0.0.1  search.oecis.test
127.0.0.1  oecis.test
127.0.0.1  kratos.oecis.test
127.0.0.1  hydra.oecis.test
127.0.0.1  registry.oecis.test
```
> Why .test for the development environment?
> Because .test is reserved for testing purposes and so we can use it without any implications.

### Deployment

You can now deploy the oecis stack through the fountain repo deployment and the 'dev' environment.

### CA certificate

After you have deployed cert-manager and the self-signed issuer you should add the new CA certificate to your local trust store.

```
# Get the certificate
$ kubectl get secret ca-secret -n cert-manager -o jsonpath='{.data}' | jq '."ca.crt"' --raw-output | base64 --decode > ca.crt
# Add the certificate to your local trust store
$ sudo trust anchor --store ca.crt
```


### Reset the VM

In case you need a fresh start you can simply delete the `oecis.qcow2` file and restart the VM.

### E-Mail

for development purposes a simple MailDev server is started. You can access the web interface at `http://localhost:1080/`
