{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
  };
  outputs =
    { self, nixpkgs }:
    {
      nixosModules = {
        k3s = ./modules/k3s.nix;
        vmConfiguration = ./modules/vm-configuration.nix;
        prod = ./modules/prod-configuration.nix;
        base = ./modules/configuration.nix;
        minio = ./modules/minio.nix;
      };
      nixosConfigurations = {
        test = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            self.nixosModules.k3s
            self.nixosModules.base
            self.nixosModules.vmConfiguration
            self.nixosModules.minio
          ];
        };
        prod = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            self.nixosModules.k3s
            self.nixosModules.base
            self.nixosModules.prod
            self.nixosModules.minio
          ];
        };
      };
    };
}
